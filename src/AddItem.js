import { useState, useEffect, useCallback } from "react";
import * as constants from "./contants";
import { Sprite, Container, TilingSprite } from "@inlet/react-pixi";
import items from "./items";
import * as utils from "./utils";

const AddItem = ({
  mode,
  itemType,
  defaultPosition,
  clickPosition,
  setRealPosition,
  world,
}) => {
  const [pos, setPos] = useState(defaultPosition);
  const _item = items[itemType];

  const getMapBorder = useCallback(() => {
    if (!_item) {
      return [
        [0, 0],
        [0, 0],
      ];
    }
    return world.tileDimensions
      .map((d) => [d[0] + 0.5, d[1] + 0.5])
      .map((d) =>
        d.map(
          (c) =>
            c * constants.world.tileSize * 2 -
            constants.world.placement.margin -
            _item.radius
        )
      )
      .map((d) => [-d[0], d[1]]);
  }, [_item, world.tileDimensions]);

  const mapBorder = getMapBorder();
  // console.log(mapBorder, world.tileDimensions);

  const avoidOthers = (_pos) => {
    if (!_item) {
      return [0, 0];
    }

    const otherItems = world.items.map((item) => {
      const _itemInfo = items[item.type];
      return {
        x: item.x - _itemInfo.radiusOffset[0],
        y: item.y - _itemInfo.radiusOffset[1],
        radius: _itemInfo.radius,
      };
    });

    let x = _pos[0] - _item.radiusOffset[0];
    let y = _pos[1] - _item.radiusOffset[1];

    const isInside = (_x, _y) => {
      return (
        otherItems.some(
          (oi) => Math.hypot(_x - oi.x, _y - oi.y) < oi.radius + _item.radius
        ) ||
        utils
          .toMapFrame([_x, _y].map((c, i) => c - _item.radiusOffset[i]))
          .some((c, i) => c < mapBorder[i][0] || c > mapBorder[i][1])
      );
    };

    const getForceVectors = (_x, _y) =>
      otherItems
        .filter(
          (oi) => Math.hypot(_x - oi.x, _y - oi.y) < oi.radius + _item.radius
        )
        .map((oi) => {
          const r = oi.radius + _item.radius;
          const h = Math.hypot(_x - oi.x, _y - oi.y);
          return [((_x - oi.x) / h) * (r - h), ((_y - oi.y) / h) * (r - h)];
        });

    const getMapForceVectors = (_x, _y) => {
      const mapCoords = utils.toMapFrame(
        [_x, _y].map((c, i) => c - _item.radiusOffset[i])
      );

      const [dx, dy] = mapCoords
        .map((c, i) => utils.clamp(c, mapBorder[i][0], mapBorder[i][1]))
        .map((l, i) => (l - mapCoords[i]) * 2);

      return utils.toViewFrame([dx, dy]);
    };

    let i = 0;
    while (isInside(x, y) && i < 1000) {
      x = x + ((Math.random() - 0.5) * i) / 100;
      y = y + ((Math.random() - 0.5) * i) / 100;
      const [dx, dy] = [
        ...getForceVectors(x, y),
        getMapForceVectors(x, y),
      ].reduce((v, n) => [v[0] + n[0], v[1] + n[1]]);
      x += dx;
      y += dy;
      i++;
    }
    console.log(x, y, i);
    return [x + _item.radiusOffset[0], y + _item.radiusOffset[1]];
  };

  useEffect(() => {
    if (mode === constants.modes.place) {
      const _pos = avoidOthers([
        defaultPosition.x + Math.random() - 0.5,
        defaultPosition.y + Math.random() - 0.5,
      ]);
      // console.log("start placement", _pos);
      setPos(_pos);
      if (setRealPosition) {
        setRealPosition(_pos);
      }
    }
  }, [mode, setPos]);

  useEffect(() => {
    if (!clickPosition || !_item) {
      return;
    }
    const clampToMap = (_pos) =>
      _pos &&
      utils
        .toViewFrame(
          utils
            .toMapFrame(_pos.map((c, i) => c - _item.radiusOffset[i]))
            .map((c, i) => utils.clamp(c, mapBorder[i][0], mapBorder[i][1]))
        )
        .map((c, i) => c + _item.radiusOffset[i]);

    const _pos = avoidOthers(clampToMap(clickPosition));

    setPos([_pos[0] || 0, _pos[1] || 0]);

    if (setRealPosition) {
      setRealPosition(_pos);
    }
  }, [
    _item?.radius,
    _item?.radiusOffset,
    clickPosition,
    setRealPosition,
    world.items,
    world.tileCount,
  ]);

  // useEffect(() => {
  //   console.log(pos);
  // }, [pos]);

  if (mode !== constants.modes.place) {
    return null;
  }

  return (
    <Sprite
      {..._item}
      alpha={0.4}
      zIndex={constants.renderLayers.foreGround}
      position={pos || [0, 0]}
    />
  );
};

export default AddItem;
