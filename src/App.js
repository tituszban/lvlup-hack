import { useState, useEffect, useCallback, useRef } from "react";
import * as PIXI from "pixi.js";
import Home from "./Home";
import Visiting from "./Visiting";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/:mapId">
          <Visiting />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
