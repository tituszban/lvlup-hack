import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Rating from "@material-ui/lab/Rating";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/Star";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import items from "./items";
import { itemTypes } from "./contants";

const PersonalContent = ({ rating, setRating, text, setText, onNext }) => {
  return (
    <>
      <DialogTitle id="simple-dialog-title">
        {rating === null
          ? "Rate your mood today"
          : "Think about what you felt went right or wrong today? Why did you choose the mood score that you did?"}
      </DialogTitle>
      <DialogContent>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ padding: 10 }}
        >
          <Grid item style={{ paddingBottom: 20, textAlign: "center" }} xs={12}>
            <Rating
              size="large"
              value={rating}
              onChange={(event, newValue) => {
                setRating(newValue);
              }}
              icon={<StarIcon fontSize="inherit" />}
              name="rating"
            />
          </Grid>
          {rating !== null && (
            <>
              <Grid item style={{ paddingBottom: 20 }} xs={12}>
                <TextField
                  id="outlined-multiline-static"
                  value={text}
                  onChange={(e) => setText(e.target.value)}
                  multiline
                  rows={6}
                  variant="outlined"
                  fullWidth
                  name="personal"
                  helperText="If anyone visits your park today, they will be able to read this and send you a message about your day"
                />
              </Grid>
              <Grid
                item
                xs={12}
                style={{ paddingBottom: 10, textAlign: "center" }}
              >
                <Button onClick={onNext} variant="contained" color="primary">
                  Next
                </Button>
              </Grid>
            </>
          )}
        </Grid>
      </DialogContent>
    </>
  );
};

const Encourigement = ({ onNext }) => {
  return (
    <>
      <DialogTitle>
        Well done! Another day that you have taken time to reflect on your
        feelings. Spending more time decorating your Memory Park will help you
        process your daily stresses and anxieties!
      </DialogTitle>
      <DialogContent>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ padding: 10 }}
        >
          <Grid item xs={12} style={{ paddingBottom: 10, textAlign: "center" }}>
            <Button onClick={onNext} variant="contained" color="primary">
              Pick a tree for today
            </Button>
          </Grid>
        </Grid>
      </DialogContent>
    </>
  );
};

const PickATree = ({ onSubmit }) => {
  return (
    <>
      <DialogTitle id="simple-dialog-title">Pick a tree for today</DialogTitle>
      <DialogContent>
        <Grid container>
          {Object.keys(items)
            .map((k) => ({ ...items[k], id: k }))
            .filter((item) => item.type === itemTypes.daily)
            .map((item, i) => (
              <Grid item key={i} xs={6}>
                <Button onClick={() => onSubmit(item.id)}>
                  <img
                    src={item.image}
                    alt={item.title}
                    style={{ height: 150, width: "100%", objectFit: "contain"}}
                  />
                </Button>
              </Grid>
            ))}
        </Grid>
      </DialogContent>
    </>
  );
};

const DailyDialog = ({ open, onSubmit }) => {
  const [rating, setRating] = useState(null);
  const [text, setText] = useState("");
  const [status, setStatus] = useState(0);

  const clear = () => {
    setRating(null);
    setText("");
    setStatus(0);
  };


  const handleSubmit = (selectedTree) => {
    setTimeout(function () { //delayed execution
      clear();
    }.bind(this), 250)
    
    onSubmit({ rating, text, item: selectedTree });
  };

  const getContent = (_status) => {
    switch (_status) {
      case 0:
        return (
          <PersonalContent
            rating={rating}
            setRating={setRating}
            text={text}
            setText={setText}
            onNext={() => setStatus(1)}
          />
        );
      case 1:
        return <Encourigement onNext={() => setStatus(2)} />;
      case 2:
        return <PickATree onSubmit={handleSubmit} />;
      default:
        return null;
    }
  };

  return (
    <Dialog
      aria-labelledby="simple-dialog-title"
      open={open}
      fullWidth
    >
      {getContent(status)}
    </Dialog>
  );
};

export default DailyDialog;
