import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Rating from "@material-ui/lab/Rating";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/Star";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import items from "./items";
import { itemTypes } from "./contants";

const SmallerPromptInput = ({ onNext, title, text, setText }) => {
  return (
    <>
      <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ padding: 10 }}
        >
          <Grid item style={{ paddingBottom: 20 }} xs={12}>
            <TextField
              id="outlined-multiline-static"
              value={text}
              onChange={(e) => setText(e.target.value)}
              multiline
              rows={6}
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} style={{ paddingBottom: 10, textAlign: "center" }}>
            <Button onClick={onNext} variant="contained" color="primary">
              Pick a decoration
            </Button>
          </Grid>
        </Grid>
      </DialogContent>
    </>
  );
};

const DecorationSelector = ({ prompts, onSelect }) => {
  const style = { paddingBottom: 5, textAlign: "center" };
  return (
    <>
      <DialogTitle>Add to your park</DialogTitle>
      <DialogContent>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ padding: 10 }}
        >
          {[...prompts, "What are you grateful for in your life?"].map(
            (text, i) => (
              <Grid item xs={12} style={style} key={text}>
                <Button
                  onClick={() => onSelect(text)}
                  fullWidth
                  variant="outlined"
                >
                  {text}
                </Button>
              </Grid>
            )
          )}
          <Grid item xs={12} style={style}>
            <Button fullWidth variant="outlined" disabled>
              Create a memory (You have 0 Memories available to create)
            </Button>
          </Grid>
        </Grid>
      </DialogContent>
    </>
  );
};

const PickADecoration = ({ onSubmit }) => {
  return (
    <>
      <DialogTitle>Pick a decoration</DialogTitle>
      <DialogContent>
        <Grid container>
          {Object.keys(items)
            .map((k) => ({ ...items[k], id: k }))
            .filter((item) => item.type === itemTypes.decoration)
            .map((item, i) => (
              <Grid item key={i} xs={6}>
                <Button onClick={() => onSubmit(item.id)}>
                  <img
                    src={item.image}
                    alt={item.title}
                    style={{ height: 150, width: "100%", objectFit: "contain"}}
                  />
                </Button>
              </Grid>
            ))}
        </Grid>
      </DialogContent>
    </>
  );
};

const promptOptions = [
  "What's a smell you remember from today?",
  "Where did you feel most comfortable today?",
  "If you could eat anything tomorrow, what would it be?",
  "What are you looking forward to the most right now?",
];

function shuffleArray(array) {
  const _array = [...array];
  for (var i = _array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = _array[i];
    _array[i] = _array[j];
    _array[j] = temp;
  }
  return _array;
}

const randomPrompts = (n = 2) => shuffleArray(promptOptions).slice(0, n);

const DecorationDialog = ({ open, onSubmit, onClose }) => {
  const [status, setStatus] = useState(0);
  const [text, setText] = useState("");
  const [prompt, setPrompt] = useState("");
  const [prompts, setPrompts] = useState(randomPrompts());

  const clear = () => {
    setStatus(0);
    setText("");
    setPrompts(randomPrompts());
  };

  const handleClose = () => {
    setTimeout(
      function () {
        //delayed execution
        clear();
      }.bind(this),
      250
    );
    onClose();
  };

  const handleSubmit = (selectedDecoration) => {
    setTimeout(
      function () {
        //delayed execution
        clear();
      }.bind(this),
      250
    );

    onSubmit({ text, prompt, item: selectedDecoration });
  };

  const getContent = (_status) => {
    switch (_status) {
      case 0:
        return (
          <DecorationSelector
            onSelect={(_prompt) => {
              setPrompt(_prompt);
              setStatus(1);
            }}
            prompts={prompts}
          />
        );
      case 1:
        return (
          <SmallerPromptInput
            text={text}
            setText={setText}
            title={prompt}
            onNext={() => setStatus(2)}
          />
        );
      case 2:
        return <PickADecoration onSubmit={handleSubmit} />;
      default:
        return null;
    }
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
      fullWidth
    >
      {getContent(status)}
    </Dialog>
  );
};

export default DecorationDialog;
