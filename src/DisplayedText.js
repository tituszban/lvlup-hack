import { useState, useEffect, useCallback, useRef } from "react";
import { Text } from "@inlet/react-pixi";
import * as PIXI from "pixi.js";

const DisplayedText = ({ position, item, size }) => {

  return (
    <>
      <Text
        text={item?.prompt || ""}
        anchor={[0.5, 0]}
        position={position}
        style={
          new PIXI.TextStyle({
            align: "center",
            fontFamily: "roboto, sans-serif",
            fontWeight: 600,
            fontSize: 28,
            wordWrap: true,
            wordWrapWidth: Math.min(size.width * 0.95, 600),
          })
        }
      />

      <Text
        text={item?.text || ""}
        anchor={[0.5, 0]}
        position={[position[0], position[1] + 70]}
        style={
          new PIXI.TextStyle({
            align: "center",
            fontFamily: "roboto, sans-serif",
            fontWeight: 400,
            fontSize: 24,
            wordWrap: true,
            wordWrapWidth: Math.min(size.width * 0.95, 700),
          })
        }
      />
    </>
  );
};

export default DisplayedText;
