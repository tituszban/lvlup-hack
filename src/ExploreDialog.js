import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Rating from "@material-ui/lab/Rating";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/Star";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import items from "./items";
import { itemTypes } from "./contants";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Tooltip from "@material-ui/core/Tooltip";

const ExploreScreen = ({ onSelect }) => {
  return (
    <>
      <DialogTitle id="simple-dialog-title">Activities</DialogTitle>
      <DialogContent>
        <Tooltip title="Not available in MVP" interactive>
          <List>
            <ListItem button disabled>
              <ListItemText primary="Search in entries" />
            </ListItem>

            <ListItem button disabled>
              <ListItemText primary="Challenges" />
            </ListItem>
          </List>
        </Tooltip>
      </DialogContent>
    </>
  );
};

const ExploreDialog = ({ open, onSubmit, onClose }) => {
  const [status, setStatus] = useState(0);

  const clear = () => {
    setStatus(0);
  };

  const handleClose = () => {
    onClose();
  };

  const handleSubmit = (selectedItem) => {
    onSubmit({ item: selectedItem });

    setTimeout(
      function () {
        //delayed execution
        clear();
      }.bind(this),
      250
    );
  };

  const getContent = (_status) => {
    switch (_status) {
      case 0:
        return <ExploreScreen onSelect={handleSubmit} />;
      default:
        return null;
    }
  };

  return (
    <Dialog
      aria-labelledby="simple-dialog-title"
      open={open}
      fullWidth
      onClose={handleClose}
    >
      {getContent(status)}
    </Dialog>
  );
};

export default ExploreDialog;
