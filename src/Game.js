import { useState, useRef, useCallback, useEffect } from "react";
import { Sprite, Container, useApp } from "@inlet/react-pixi";

// import tile from "./assets/Tile.png";
import background from "./assets/Background.png";
// import Grid from "@material-ui/core/Grid";
// import Button from "@material-ui/core/Button";
import * as PIXI from "pixi.js";
import * as canvasExtract from "@pixi/canvas-extract";
import { CanvasRenderer } from "@pixi/canvas-renderer";
import World from "./World";
import { useMouseEvents } from "./gameHooks";
import AddItem from "./AddItem";

PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

const Game = ({
  center,
  mode,
  world,
  addItemType,
  setItemRealPosition,
  setSelectedItem,
  setScreenshotBlob,
  highlightIndex,
  setDimension,
  showExpansion,
}) => {
  const [clickPosition, setClickPosition] = useState(null);
  const app = useApp();
  const stageRef = useRef();

  const savePicture = () => {
    if (!stageRef.current) {
      return;
    }

    app.renderer.extract.canvas(stageRef.current).toBlob((blob) => {
      // console.log(blob);
      setScreenshotBlob(blob);
      // uploadBlob(blob).then(console.log);
    });
  };

  useEffect(() => {
    savePicture();
  }, [world, stageRef?.current]);

  const handlePressEvent = (click) => {
    setClickPosition(click);
    setSelectedItem(null);
    // savePicture();
  };
  const bind = useMouseEvents(handlePressEvent);

  return (
    <Container>
      <Sprite
        image={background}
        anchor={0.5}
        position={[center.x, center.y]}
        scale={30}
        {...bind}
      />
      <Container scale={1} sortableChildren={true} ref={stageRef}>
        <World
          x={center.x}
          y={center.y}
          world={world}
          setSelectedItem={setSelectedItem}
          highlightIndex={highlightIndex}
          setDimension={setDimension}
          showExpansion={showExpansion}
        />
        <AddItem
          mode={mode}
          defaultPosition={center}
          world={world}
          clickPosition={clickPosition}
          itemType={addItemType}
          setRealPosition={setItemRealPosition}
        />
      </Container>
      {/* <WorldExpanion world={world}/> */}
    </Container>
  );
};

export default Game;
