import { useState, useRef, useCallback, useEffect } from "react";
import { Sprite } from "@inlet/react-pixi";
import { modes } from "./contants";
import container from "./assets/ui/ButtonArea.png";
import plantButton from "./assets/ui/dailyreflection-btn.png";
import benchButton from "./assets/ui/moment-btn.png";
import wideContainer from "./assets/ui/ButtonAreaWide.png";
import cancelButton from "./assets/ui/CancelButton.png";
import acceptButton from "./assets/ui/confirm-btn.png";
import exitButton from "./assets/ui/leave-btn.png";
import { GlowFilter } from "@pixi/filter-glow";

const SpriteButton = ({ onClick, ...props }) => {
  const [isPressed, setIsPressed] = useState(false);

  return (
    <Sprite
      interactive
      alpha={isPressed ? 0.8 : 1}
      pointerdown={() => setIsPressed(true)}
      pointerup={() => {
        if (isPressed) {
          setIsPressed(false);
          onClick();
        }
      }}
      pointerupoutside={() => setIsPressed(false)}
      {...props}
    />
  );
};

const GameUi = ({
  position,
  onClick,
  show,
  mode,
  showDaily,
  readonly,
  glowing,
}) => {
  if (!show) {
    return null;
  }

  const props = {
    scale: 0.4,
    anchor: 0.5,
    position: [position[0], position[1] - 70],
  };

  const sprite = readonly
    ? exitButton
    : mode === modes.place
    ? acceptButton
    : showDaily
    ? plantButton
    : benchButton;

  const action = readonly
    ? () => onClick("exit")
    : mode === modes.place
    ? () => onClick("accept")
    : () => onClick("plant");

  return (
    <>
      <SpriteButton
        image={sprite}
        {...props}
        onClick={action}
        filters={
          glowing
            ? [
                new GlowFilter({
                  color: 0xeae018,
                }),
              ]
            : []
        }
      />
    </>
  );
};

export default GameUi;
