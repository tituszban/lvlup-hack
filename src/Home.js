import { useState, useEffect, useCallback, useRef, useReducer } from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import WorldView from "./WorldView";

import { useResize, useLocalStorageState, useTutorial } from "./gameHooks";
import { itemTypes, modes } from "./contants";
import Typography from "@material-ui/core/Typography";

const buttonPadding = 40;

const worldExpansion = [...Array(1000).keys()].reduce(
  (obj, i) => ({ ...obj, [Math.ceil(5 * Math.pow(1.3, i))]: 5 + i * 2 }),
  {}
);

const startingWorld = {
  tileCount: 3,
  tileDimensions: [
    [1, 1],
    [1, 1],
  ],
  items: [
    {
      type: "fountain",
      x: 0,
      y: -50,
    },
    {
      type: "signpost",
      x: 180,
      y: 20,
    },
    {
      type: "crate",
      x: -150,
      y: -100,
    },
  ],
};

const startingDay = {
  day: 1,
  dailyAdded: false,
};

function Home() {
  const size = useResize();
  const [selectedItem, setSelectedItem] = useState(null);

  const [world, setWorld] = useLocalStorageState("world", startingWorld);

  const [day, setDay] = useLocalStorageState("day", startingDay);

  const [tutorialState, dispatchTutorial] = useTutorial(world, day, true);
  const [mode, setMode] = useState(modes.view);

  const addItem = (_item, _itemPosition) => {
    if (!dispatchTutorial({ type: "addItem", addedItem: _item })) {
      return;
    }

    if (_item.itemType === itemTypes.daily) {
      setDay({ ...day, dailyAdded: true });
    }

    setWorld({
      ...world,
      items: [
        ...(world.items || []),
        {
          type: _item.item,
          x: _itemPosition[0],
          y: _itemPosition[1],
          ..._item,
        },
      ],
    });
  };

  const handleTutorialClick = (_item) => {
    return dispatchTutorial({ type: "click", target: _item });
  };

  const handleNextDay = () => {
    const _newDay = day.day + 1;

    if (!dispatchTutorial({ type: "nextDayClick", nextDay: _newDay })) {
      return;
    }

    setDay({ ...day, day: _newDay, dailyAdded: false });
    if (worldExpansion[_newDay]) {
      // setWorld({ ...world, tileCount: worldExpansion[_newDay] });
      setMode(modes.expansion);
    }
  };

  const handleReset = () => {
    setWorld(startingWorld);
    setDay(startingDay);
    dispatchTutorial({ type: "reset" });
    setMode(modes.view);
  };

  useEffect(() => {
    console.log(selectedItem);
  }, [selectedItem]);

  const handleSetDimension = (_dimensions) => {
    console.log(_dimensions);
    if (!dispatchTutorial({ type: "setDimensions", dimensions: _dimensions })) {
      return;
    }

    setWorld({ ...world, tileDimensions: _dimensions });
    setMode(modes.view);
  };

  const handleModeChange = (_mode) => {
    setMode(_mode);
    dispatchTutorial({ type: "modeChange", target: _mode });
  };

  return (
    <div
      style={{
        width: size.width,
        height: size.height,
        backgroundColor: "lightblue",
      }}
    >
      <Grid container>
        <Grid item xs={12}>
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <WorldView
              world={world}
              day={day}
              addItem={addItem}
              setSelectedItem={setSelectedItem}
              onTutorialClick={handleTutorialClick}
              selectedItem={selectedItem}
              size={{ width: size.width, height: size.height - buttonPadding }}
              onReturn={() => console.log("return")}
              tutorial={tutorialState.tutorial}
              supressDialogs={tutorialState.supressClick}
              highlightIndex={tutorialState.highlightIndex}
              setDimension={handleSetDimension}
              mode={mode}
              setMode={handleModeChange}
            />
            <Grid item xs={12}>
              <Grid
                container
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <Grid item>
                  <Grid
                    container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                  >
                    <Grid item style={{ paddingRight: 5, paddingLeft: 5 }}>
                      <Typography
                        variant={"button"}
                      >{`Day ${day.day}`}</Typography>
                    </Grid>
                    <Grid item>
                      <Button onClick={handleNextDay}>Next day</Button>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Button onClick={handleReset}>Reset</Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default Home;
