import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Rating from "@material-ui/lab/Rating";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/Star";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import items from "./items";
import { itemTypes } from "./contants";


const InventoryScreen = ({ onSelect }) => {
  return (
    <>
      <DialogTitle id="simple-dialog-title">Your items</DialogTitle>
      <DialogContent>
        <Grid container>
          {Object.keys(items)
            .map((k) => ({ ...items[k], id: k }))
            .filter((item) => item.type === itemTypes.decoration)
            .map((item, i) => (
              <Grid item key={i} xs={4}>
                <img
                  src={item.image}
                  alt={item.title}
                  style={{ height: 100, width: "100%", objectFit: "contain"}}
                />

              </Grid>
            ))}
        </Grid>
      </DialogContent>
    </>
  );
};

const InventoryDialog = ({ open, onSubmit, onClose }) => {
  const [status, setStatus] = useState(0);

  const clear = () => {
    setStatus(0);
  };

  const handleClose = () => {
    onClose();

  };


  const handleSubmit = (selectedItem) => {
    onSubmit({ item: selectedItem });

    setTimeout(function () { //delayed execution
      clear();
    }.bind(this), 250)
  };

  const getContent = (_status) => {
    switch (_status) {
      case 0:
        return (
          <InventoryScreen
            onSelect={handleSubmit}
          />
        );
      default:
        return null;
    }
  };

  return (
    <Dialog
      aria-labelledby="simple-dialog-title"
      open={open}
      fullWidth
      onClose={handleClose}
    >
      {getContent(status)}
    </Dialog>
  );
};

export default InventoryDialog;
