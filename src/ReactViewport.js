import React from "react";
import * as PIXI from "pixi.js";
import { PixiComponent, useApp } from "@inlet/react-pixi";
import { Viewport as PixiViewport } from "pixi-viewport";

const PixiComponentViewport = PixiComponent("Viewport", {
  create: (props) => {
    const viewport = new PixiViewport({
      screenWidth: props.width,
      screenHeight: props.height,
      worldWidth: props.width,
      worldHeight: props.height,
      ticker: props.app.ticker,
      interaction: props.app.renderer.plugins.interaction,
    });

    viewport
      .pinch()
      .wheel()
      .drag()
      .clampZoom({
        minWidth: 500,
        minHeight: 500,
        maxWidth: 800 * props.tileCount,
        maxHeight: 800 * props.tileCount,
      })
      .snap(0, 0, { time: 0, removeOnComplete: true });

    return viewport;
  },
  applyProps: (viewport, oldProps, props) => {
    // console.log(props);
    viewport.screenWidth = props.width;
    viewport.screenHeight = props.height;
    viewport.worldWidth = props.width;
    viewport.worldHeight = props.height;
    viewport.clampZoom({
      minWidth: 500,
      minHeight: 500,
      maxWidth: 800 * props.tileCount,
      maxHeight: 800 * props.tileCount,
    });
  },
});

const Viewport = (props) => {
  const app = useApp();
  return <PixiComponentViewport app={app} {...props} />;
};

export default Viewport;
