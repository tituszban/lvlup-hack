import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import PersonIcon from "@material-ui/icons/Person";
import AddIcon from "@material-ui/icons/Add";
import Typography from "@material-ui/core/Typography";
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import { blue } from "@material-ui/core/colors";
import { saveMap, getMapList, getMap } from "./firestore";
import { TextField } from "@material-ui/core";
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

const ParkListItem = (props) => {
  const classes = useStyles();
  console.log("ParkListItem props", props);
  return (
    <ListItem button onClick={props.onClick}>
      <ListItemAvatar>
        <Avatar src={props.url} variant="square" className={classes.square} />
      </ListItemAvatar>
      <ListItemText primary={props.name} />
    </ListItem>
  );
};
const FriendListItem = (props) => {
  const classes = useStyles();
  console.log("ParkListItem props", props);
  return (

    <ListItem button onClick={props.onClick}>
      <ListItemAvatar>
        <Avatar src={props.url} variant="square" className={classes.square} />
      </ListItemAvatar>
      <ListItemText primary={props.name} />
    </ListItem>
  );
};

const pages = {
  select: 0,
  load: 1,
  save: 2,
};

const LoadPark = ({ onLoad }) => {
  const [mapList, setMapList] = useState([]);
  const handleParkListItemClick = (id) => {
    // console.log("Clicked", id);
    onLoad(id);
    // handleClose();
    // console.log(getMap(id))
    // getMap(id).then((m) => onLoad(m));
  };

  useEffect(() => {
    getMapList().then(setMapList);
  }, []);

  return (
    <>
      <DialogTitle id="simple-dialog-title">Load a park</DialogTitle>
      <DialogContent>
        <List style={{ maxHeight: 200 }}>
          {[...mapList].map((park) => (
            <ParkListItem
              {...park}
              onClick={() => handleParkListItemClick(park.id)}
            />
          ))}
        </List>
      </DialogContent>
    </>
  );
};

const SharePark = ({ world, screenshotBlob, onComplete }) => {
  const [parkName, setParkName] = useState("");

  const handleSave = () => {
    if (parkName) {
      // console.log(screenshotBlob);
      saveMap(
        parkName,
        {
          ...world,
          items: world.items.map((item) => ({
            ...item,
            prompt: null,
            text: null,
          })),
          tileDimensions: [
            ...world.tileDimensions[0],
            ...world.tileDimensions[1],
          ],
        },
        screenshotBlob
      );
      onComplete();
    }
  };

  return (
    <>
      <DialogTitle>Share your park</DialogTitle>
      <DialogContent>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ padding: 10 }}
        >
          <Grid item style={{ paddingBottom: 20 }} xs={12}>
            <TextField
              value={parkName}
              onChange={(event) => setParkName(event.target.value)}
              label="Name your Park"
              type="search"
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} style={{ paddingBottom: 10, textAlign: "center" }}>
            <Button onClick={handleSave} variant="contained" color="primary">
              Save
            </Button>

          </Grid>

        </Grid>

        <Grid>

        </Grid>
      </DialogContent>
    </>
  );
};

const SharingSelector = ({ setPage, loadPark, readonly }) => {
  return (
    <>
      {/* <DialogTitle id="simple-dialog-title">select</DialogTitle> */}
      <DialogContent>
        <LoadPark onLoad={loadPark} />
        <DialogTitle id="simple-dialog-title">Friends' parks</DialogTitle>
        <DialogContent>
          <Tooltip title="Not available in MVP" interactive>

            <List style={{ maxHeight: 200 }}>

              <ListItem button disabled>
                <ListItemAvatar>
                  <Avatar>
                    <AddIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Add friend" />
              </ListItem>
            </List>
          </Tooltip>

        </DialogContent>

        {!readonly && (
          <Button
            fullWidth
            variant="contained"
            color="primary"
            onClick={() => setPage(pages.save)}
            style={{ marginTop: 40 }}
          >
            Share your park
          </Button>
        )}
      </DialogContent>
    </>
  );
};

function SharingDialog({
  world,
  screenshotBlob,
  open,
  onClose,
  loadPark,
  readonly,
}) {
  const classes = useStyles();

  const [page, setPage] = useState(pages.select);

  const handleClose = () => {
    onClose();
    setTimeout(
      function () {
        //delayed execution
        setPage(pages.select);
      }.bind(this),
      250
    );
  };

  const getContent = (_page) => {
    switch (_page) {
      case pages.select:
        return (
          <SharingSelector
            setPage={setPage}
            loadPark={loadPark}
            readonly={readonly}
          />
        );
      case pages.save:
        return (
          <SharePark
            world={world}
            screenshotBlob={screenshotBlob}
            onComplete={handleClose}
          />
        );
      default:
        return null;
    }
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
      fullWidth
    >
      {getContent(page)}
    </Dialog>
  );
}

export default SharingDialog;
