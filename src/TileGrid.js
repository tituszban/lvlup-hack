// import { useState, useEffect, useRef, useCallback } from "react";
import { Sprite } from "@inlet/react-pixi";
import tile from "./assets/Tile.png";
import { renderLayers, world } from "./contants";

const toIso = (cartX, cartY) => {
  const isoX = cartX + cartY;
  const isoY = cartY - cartX;
  return [isoX, isoY];
};

const range = (from, to) =>
  [...Array(to - from + 1).keys()].map((v) => v + from);

const generateCoords = (dimensions) =>
  range(-dimensions[1][0], dimensions[1][1])
    .map((i) =>
      range(-dimensions[0][0], dimensions[0][1]).map((j) => toIso(i, j))
    )
    .reduce((arr, it) => [...arr, ...it], []);

const generateExtensionCoords = (dimensions) => {
  return [
    range(-dimensions[1][0], dimensions[1][1]).map((i) =>
      toIso(i, -dimensions[0][0] - 1)
    ),
    range(-dimensions[1][0], dimensions[1][1]).map((i) =>
      toIso(i, dimensions[0][1] + 1)
    ),
    range(-dimensions[0][0], dimensions[0][1]).map((j) =>
      toIso(-dimensions[1][0] - 1, j)
    ),
    range(-dimensions[0][0], dimensions[0][1]).map((j) =>
      toIso(dimensions[1][1] + 1, j)
    ),
  ];
};

const TileGrid = ({ dimensions, x, y, setDimension, showExpansion }) => {
  // console.log(generateCoords(countX, countY));
  const sideClick = (side) => {
    console.log(side, dimensions);
    setDimension([
      [
        dimensions[0][0] + (side === 0),
        dimensions[0][1] + (side === 1),
      ],
      [
        dimensions[1][0] + (side === 2),
        dimensions[1][1] + (side === 3),
      ]
    ])
  };
  return (
    <>
      {generateCoords(dimensions).map(([i, j]) => (
        <Sprite
          key={`${i}${j}`}
          image={tile}
          scale={1}
          anchor={0.5}
          x={x + i * world.tileSpacingX}
          y={y + j * world.tileSpacingY}
          zIndex={j * 1000 + i + renderLayers.background}
        />
      ))}
      {showExpansion &&
        generateExtensionCoords(dimensions).map((side, side_i) =>
          side.map(([i, j]) => (
            <Sprite
              key={`${i}${j}`}
              image={tile}
              scale={1}
              anchor={0.5}
              x={x + i * world.tileSpacingX}
              y={y + j * world.tileSpacingY}
              zIndex={j * 1000 + i + renderLayers.background}
              alpha={0.5}
              pointerup={() => sideClick(side_i)}
              interactive
            />
          ))
        )}
    </>
  );
};

export default TileGrid;
