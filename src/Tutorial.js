import { useState, useEffect, useRef, useCallback } from "react";
import { Sprite, Container, Text, Graphics } from "@inlet/react-pixi";
import * as PIXI from "pixi.js";
import { renderLayers } from "./contants";
import Avatar from "./assets/GhostDeer.png";

const Tutorial = ({ position, height, size, text }) => {
    const [x, y] = position;
    const width = Math.min(size.width * 0.95, 600);
  const draw = useCallback((g) => {
    g.clear();
    g.lineStyle(2, 0x5AA843, 1);
    g.beginFill(0xffffff, 0.8);
    g.drawRoundedRect(x - width / 2, y - height - 150, width, height, 15);
    g.endFill();
  }, [x, y, width, height]);

  return (
    <>
      <Graphics draw={draw} />
      <Text
        text={text}
        anchor={[0, 0]}
        position={[x - width / 2 + 20, y - height - 130]}
        style={
          new PIXI.TextStyle({
            align: "left",
            fontFamily: "roboto, sans-serif",
            fontWeight: 400,
            fontSize: 24,
            wordWrap: true,
            wordWrapWidth: width - 40,
          })
        }
      />
      <Sprite
        image={Avatar}
        zIndex={renderLayers.foreGround + 100}
        position={[x + width / 2 - 45, y]}
        scale={0.18}
        anchor={[0.5, 0.9]}
      />
    </>
  );
};

export default Tutorial;
