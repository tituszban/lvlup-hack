import { useState, useEffect, useCallback, useRef } from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import WorldView from "./WorldView";

import { useResize } from "./gameHooks";

import { useParams, useHistory } from "react-router-dom";
import { getMap } from "./firestore";
import { modes } from "./contants";

function Visiting() {
  const size = useResize();
  const [selectedItem, setSelectedItem] = useState(null);

  const { mapId } = useParams();
  const history = useHistory();

  const [world, setWorld] = useState(null);

  useEffect(() => {
    getMap(mapId).then((_world) => {
      if (!_world) {
        console.error("invalid world. Redirecting");
        history.replace("/");
        return;
      }
      console.log(_world);
      setSelectedItem({ prompt: `You are visiting: ${_world.name}` });
      setWorld({
        ..._world.data,
        tileDimensions: _world.data.tileDimensions && [
          _world.data.tileDimensions.slice(0, 2),
          _world.data.tileDimensions.slice(2, 4),
        ],
      });
    });
  }, [mapId]);

  const day = {
    day: 0,
    dailyAdded: false,
  };

  return (
    <div
      style={{
        width: size.width,
        height: size.height,
        backgroundColor: "lightblue",
      }}
    >
      <Grid container>
        <Grid item xs={12}>
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <WorldView
              world={world}
              day={day}
              addItem={() => {}}
              setSelectedItem={() => {}}
              selectedItem={selectedItem}
              size={{ width: size.width, height: size.height - 4 }}
              readonly
              onReturn={() => history.push("/")}
              onTutorialClick={() => true}
              setDimension={() => {}}
              mode={modes.view}
              setMode={() => {}}
            />
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default Visiting;
