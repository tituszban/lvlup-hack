import { useState, useEffect, useRef, useCallback } from "react";
import { Sprite, Container, TilingSprite } from "@inlet/react-pixi";
import TileGrid from "./TileGrid";
import { renderLayers } from "./contants";
import items from "./items";
import { GlowFilter } from "@pixi/filter-glow";

const Item = ({ x, y, item, setSelectedItem, glowing, ...props }) => {
  const _item = items[item.type];
  return (
    <>
      {_item.shadow && (
        <Sprite
          zIndex={y + renderLayers.items - 100}
          position={[x, y]}
          {..._item.shadow}
        />
      )}
      <Sprite
        {..._item}
        position={[x, y]}
        zIndex={y + renderLayers.items}
        {...props}
        interactive
        pointerup={() => {
          // console.log(item);
          setSelectedItem(item);
        }}
        // pointerover={() => console.log("in")}
        // pointerout={() => console.log("out")}
        filters={
          glowing
            ? [
                new GlowFilter({
                  color: 0xeae018,
                }),
              ]
            : []
        }
      />
    </>
  );
};

const World = ({ x, y, world, setSelectedItem, highlightIndex, setDimension, showExpansion }) => {
  const w = Math.floor(world.tileCount / 2);
  const _dimensions = world.tileDimensions || [
    [w, w],
    [w, w],
  ];

  return (
    <>
      <TileGrid dimensions={_dimensions} x={x} y={y} showExpansion={showExpansion} setDimension={setDimension} />
      {world?.items?.map((item, i) => (
        <Item
          x={item.x + x}
          y={item.y + y}
          item={item}
          key={i}
          setSelectedItem={setSelectedItem}
          glowing={i === highlightIndex}
        />
      ))}
    </>
  );
};

export default World;
