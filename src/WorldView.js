import { useState, useEffect, useCallback, useRef } from "react";
import { Stage } from "@inlet/react-pixi";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import * as PIXI from "pixi.js";
import Game from "./Game";
import ReactViewport from "./ReactViewport";
import background from "./assets/Background.png";
import GameUi from "./GameUi";
import SharingDialog from "./SharingDialog";
import DailyDialog from "./DailyDialog";
import DecorationDialog from "./DecorationDialog";
import DisplayedText from "./DisplayedText";
import Tutorial from "./Tutorial";
import InventoryDialog from "./InventoryDialog";
import ExploreDialog from "./ExploreDialog";

import { useResize, useLocalStorageState } from "./gameHooks";
import { modes, itemTypes } from "./contants";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router";

const dialogs = {
  none: 0,
  daily: 1,
  decoration: 2,
  sharing: 3,
  inventory: 4,
  explore: 5,
};

const WorldView = ({
  world,
  day,
  addItem,
  selectedItem,
  setSelectedItem,
  readonly,
  onReturn,
  size,
  tutorial,
  onTutorialClick,
  highlightIndex,
  setDimension,
  mode,
  setMode,
}) => {
  const [dialog, setDialog] = useState(dialogs.none);
  const [addItemData, setAddItemData] = useState({});
  const [itemPosition, setItemPosition] = useState(null);
  const [screenshotBlob, setScreenshotBlob] = useState(null);
  const history = useHistory();

  const center = { x: 0, y: 0 };

  const openDailyDialog = () => {
    setMode(modes.dialog);
    setDialog(dialogs.daily);
  };

  const handleDailySubmit = (result) => {
    setMode(modes.place);
    setDialog(dialogs.none);
    setAddItemData({
      ...result,
      itemType: itemTypes.daily,
      prompt: `Day ${day.day}`,
    });
  };

  const openDecorationDialog = () => {
    setDialog(dialogs.decoration);
  };

  const handleDecorationSubmit = (result) => {
    setMode(modes.place);
    setDialog(dialogs.none);
    setAddItemData({ ...result, itemType: itemTypes.decoration });
  };

  const handleItemSelection = (selected) => {
    console.log(selected);
    if (!onTutorialClick({ action: "itemClick", item: selected })) {
      return;
    }
    if (mode !== modes.view) {
      return;
    }
    if (selected?.type === "fountain") {
      setDialog(dialogs.sharing);
      return;
    }
    if (selected?.type === "crate" && !readonly) {
      setDialog(dialogs.inventory);
      return;
    }
    if (selected?.type === "signpost" && !readonly) {
      setDialog(dialogs.explore);
      return;
    }
    setSelectedItem(selected);
  };

  if (!world) {
    return null;
  }

  return (
    <Grid item xs={12}>
      <Stage
        width={size.width}
        height={size.height}
        options={{ backgroundColor: 0xeef1f5 }}
      >
        <ReactViewport
          width={size.width}
          height={size.height}
          tileCount={
            world.tileDimensions
              ? Math.max(...world.tileDimensions.map((d) => 1 + d[0] + d[1]))
              : world.tileCount
          }
        >
          <Game
            center={center}
            mode={mode}
            world={world}
            addItemType={addItemData.item}
            setItemRealPosition={setItemPosition}
            setSelectedItem={handleItemSelection}
            setScreenshotBlob={setScreenshotBlob}
            highlightIndex={highlightIndex}
            showExpansion={mode === modes.expansion}
            setDimension={setDimension}
          />
        </ReactViewport>
        <DisplayedText
          position={[size.width / 2, 50]}
          size={size}
          item={selectedItem}
        />
        <GameUi
          position={[size.width / 2, size.height]}
          onClick={(action) => {
            if (
              !onTutorialClick({ action: "buttonClick", buttonAction: action })
            ) {
              return;
            }
            if (action === "plant") {
              if (!day.dailyAdded) {
                openDailyDialog();
              } else {
                openDecorationDialog();
              }
            } else if (action === "accept") {
              addItem(addItemData, itemPosition);
              setMode(modes.view);
            } else if (action === "exit") {
              onReturn();
            }
          }}
          show={mode !== modes.dialog && !tutorial?.hideButton}
          mode={mode}
          showDaily={!day.dailyAdded}
          readonly={readonly}
          glowing={highlightIndex === -1}
        />
        {tutorial && (
          <Tutorial
            position={[size.width / 2, size.height - (tutorial.yOffset || 120)]}
            height={tutorial.height}
            size={size}
            text={tutorial.text}
          />
        )}
      </Stage>
      <SharingDialog
        open={dialog === dialogs.sharing}
        onClose={() => setDialog(dialogs.none)}
        world={world}
        screenshotBlob={screenshotBlob}
        readonly={readonly}
        loadPark={(id) => {
          history.push(`/${id}`);
          console.log(id);
          setDialog(dialogs.none);
        }}
      />
      <DailyDialog
        open={dialog === dialogs.daily}
        onSubmit={handleDailySubmit}
      />
      <DecorationDialog
        open={dialog === dialogs.decoration}
        onSubmit={handleDecorationSubmit}
        onClose={() => setDialog(dialogs.none)}
      />
      <InventoryDialog
        open={dialog === dialogs.inventory}
        onClose={() => setDialog(dialogs.none)}
      />
      <ExploreDialog
        open={dialog === dialogs.explore}
        onSubmit={(item) => setDialog(dialogs.none)}
        onClose={() => setDialog(dialogs.none)}
      />
    </Grid>
  );
};

export default WorldView;
