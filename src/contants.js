export const renderLayers = {
  background: 0,
  items: 100000,
  foreGround: 100000000,
};

export const modes = {
  view: 0,
  place: 1,
  dialog: 2,
  expansion: 3
};

export const world = {
  tileSize: 335,
  tileSpacingX: 335,
  tileSpacingY: 193,
  placement: {
      margin: 50,
  }
};

export const itemTypes = {
  daily: 0,
  decoration: 1,
  static: 2
}