// Firebase App (the core Firebase SDK) is always required and must be listed first
import firebase from "firebase/app";
// If you are using v7 or any earlier version of the JS SDK, you should import firebase using namespace import
// import * as firebase from "firebase/app"

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

firebase.initializeApp({
  apiKey: "AIzaSyB1otCcQu_GPQP3i_utLpnnGJah1L1KlKY",
  authDomain: "lvlup-hack.firebaseapp.com",
  projectId: "lvlup-hack",
  storageBucket: "gs://lvlup-hack.appspot.com",
});

var db = firebase.firestore();
var storage = firebase.storage().ref();

export const uploadBlob = async (blob, name) => {
  const snapshot = await storage.child(`${name}.jpg`).put(blob);
  return snapshot.ref.getDownloadURL();
};

export const saveMap = async (name, data, blob) => {
  const screenshotName = Math.random().toString(36).substring(7);
  const url = await uploadBlob(blob, screenshotName);
  try {
    const docRef = await db.collection("parks").add({
      name: name,
      data: data,
      url: url,
      created: Date.now(),
    });
    console.log("Document written with ID: ", docRef.id);
  } catch (error) {
    console.error("Error adding document: ", error);
  }
};
export const getMapList = async () => {
  var parksRef = db.collection("parks");
  var query = parksRef.orderBy("created", "desc");

  try {
    var response = await query.get();
    return response.docs.map((document) => ({
      name: document.data().name,
      id: document.id,
      url: document.data().url
    }));
  } catch (error) {
    console.log("Error getting documents: ", error);
  }
};

export const getMap = async (id) => {
  try {
    var doc = await db.collection("parks").doc(id).get();
    if (doc.exists) {
      // console.log("Document data:", doc.data());
      return doc.data();
    } else {
      // doc.data() will be undefined in this case
      console.log("No such document!");
    }
  } catch (error) {
    console.log("Error getting document:", error);
  }
};
