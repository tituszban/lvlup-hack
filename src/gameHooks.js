import { useState, useRef, useCallback, useEffect } from "react";
import { modes } from "./contants";

export const useMouseEvents = (onPressEvent) => {
  const sprite = useRef();
  const [pressed, setPressed] = useState(false);
  const [movedSinceTouch, setMovedSinceTouch] = useState(false);
  const [clickPos, setClickPos] = useState([]);

  const onDown = (e) => {
    setPressed(true);
    setMovedSinceTouch(false);
    const p = e.data.getLocalPosition(sprite.current.parent);
    // setClickPos(p);
    onPressEvent([p.x, p.y]);
  };
  // const onUp = (e) => {
  //   setPressed(false);
  //   if (!movedSinceTouch && onPressEvent) {
  //     const p = e.data.getLocalPosition(sprite.current.parent);
  //     onPressEvent([p.x, p.y]);
  //   }
  //   setMovedSinceTouch(false);
  // };

  // const onMove = useCallback(
  //   (e) => {
  //     // console.log(e.data.getLocalPosition(sprite.current.parent));
  //     const p = e.data.getLocalPosition(sprite.current.parent);
  //     if (
  //       pressed &&
  //       sprite.current &&
  //       Math.hypot(p.x - clickPos.x, p.y - clickPos.y) > 10
  //     ) {
  //       setMovedSinceTouch(true);
  //       // console.log("move", e);
  //     }
  //   },
  //   [pressed, setMovedSinceTouch]
  // );

  return {
    ref: sprite,
    interactive: true,
    pointerdown: onDown,
    // pointerup: onUp,
    // pointerupoutside: onUp,
    // pointermove: onMove,
  };
};

const getScreenSize = () => ({
  width: window.innerWidth,
  height: window.innerHeight,
});

export const useResize = () => {
  const [size, setSize] = useState(getScreenSize());

  useEffect(() => {
    const onResize = () => setSize(getScreenSize());
    window.addEventListener("resize", onResize);
    return () => window.removeEventListener("resize", onResize);
  }, []);

  return size;
};

export const useLocalStorageState = (key, defaultValue = null) => {
  const rawData = localStorage.getItem(key);
  const data = rawData ? JSON.parse(rawData) : defaultValue;
  const [state, setState] = useState(data);

  const update = (data) => {
    localStorage.setItem(key, JSON.stringify(data));
    setState(data);
  };

  return [state, update];
};

const tutorialStartingState = {
  tutorial: null,
  step: 1,
};

export const useTutorial = (world, day, enabled) => {
  const [state, setState] = useLocalStorageState(
    "tutorial",
    tutorialStartingState
  );

  const safeStates = [1, 2, 6, 7, 9, 10, 11, 12, 13, 14];

  useEffect(() => {
    var _step = state.step;
    while (!safeStates.includes(_step) && _step > 1) {
      _step -= 1;
    }
    setState({ ...state, step: _step });
    console.log(_step);
  }, []);

  const findItem = (type) =>
    world.items.findIndex((item) => item.type === type);

  const returnStates = {
    1: null,
    2: {
      tutorial: {
        height: 450,
        text: "Welcome to Memory Park!\nIt's wonderful that you decided to begin a habit of mindfulness and self-reflection. Let me guide you through your first activity. Give yourself time to relax. In your mind, walk through what happened today. Don't be afraid of recognizing a sad or anxious day, and definitely don't be shy to smile about an amazing one! Click on the button below and complete your first Daily Reflection!",
      },
      highlightIndex: -1,
    },
    3: {
      highlightIndex: null,
      tutorial: null,
    },
    4: {
      highlightIndex: null,
      tutorial: {
        text: "Well done! Now let's plant this daily journal in your park in the form of a tree. I see you already chose one! Place it wherever you like.",
        height: 180,
        yOffset: 50,
      },
    },
    5: {
      highlightIndex: -1,
      tutorial: null,
    },
    6: {
      highlightIndex: null,
      tutorial: {
        text: "Amazing! That tree will forever remember your feelings from today. You can always explore your previous thoughts by clicking on anything in your park.",
        height: 200,
        yOffset: 50,
        hideButton: true,
      },
    },
    7: {
      highlightIndex: -1,
      tutorial: {
        text: "Now that you've completed your Daily Reflection, and planted your tree, you should be able to add all sorts of other beautiful things too. You can click again on the big button and see for yourself the activities that will allow you to grow your park.",
        height: 300,
        yOffset: 120,
      },
    },
    9: {
      highlightIndex: null,
      tutorial: {
        text: "You've now embarked on your journey to a healthy mindful habit. It may look like your park is empty right now, but everything must start somewhere! Soon you'll have a lush forest with cute companions, filled with your own personality - literally, as well as figuratively.",
        height: 350,
        yOffset: 50,
        hideButton: true,
      },
    },
    10: {
      highlightIndex: findItem("crate"),
      tutorial: {
        text: "You can check available resources and unlockables in your chest",
        height: 150,
        yOffset: 50,
        hideButton: true,
      },
    },
    11: {
      highlightIndex: findItem("signpost"),
      tutorial: {
        text: "The signpost will help you find thoughts in your park and later on you'll be able to see challenges here as well!",
        height: 150,
        yOffset: 50,
        hideButton: true,
      },
    },
    12: {
      highlightIndex: findItem("fountain"),
      tutorial: {
        text: "And finally, take a look at your fountain to invite or visit friends or family. Interacting with their park can get you both some rewards!",
        height: 200,
        yOffset: 50,
        hideButton: true,
      },
    },
    13: {
      highlightIndex: null,
      tutorial: {
        text: "Remember, each thought and feeling you have defines you. Writing them down helps you cope with your issues and understand yourself better. Good luck!",
        height: 200,
        yOffset: 50,
        hideButton: true,
      },
    },
  };

  const handleAction = (action) => {
    console.log(action);
    if (action.type === "reset") {
      setState(tutorialStartingState);
    }
    if (!enabled) {
      return true;
    }
    if (action.type === "nextDayClick" && state.step <= 13) {
      return false;
    }

    if (state.step === 1) {
      if (action.type === "click") {
        setState({
          ...state,
          step: 2,
        });
      }
      return false;
    } else if (state.step === 2) {
      if (
        action?.type === "click" &&
        action?.target?.action === "buttonClick"
      ) {
        setState({
          ...state,
          step: 3,
        });
        return true;
      }
      return false;
    } else if (state.step === 3) {
      if (action.type === "modeChange" && action?.target === modes.place) {
        setState({
          ...state,
          step: 4,
        });
        return true;
      }
      return false;
    } else if (state.step === 4) {
      if (action.type === "click" && action?.target?.action === "itemClick") {
        setState({
          ...state,
          step: 5,
        });
      }
      if (action.type === "click" && action?.target?.action === "buttonClick") {
        setState({
          ...state,
          step: 6,
        });
      }
      return true;
    } else if (state.step === 5) {
      if (action.type === "click" && action?.target?.action === "buttonClick") {
        setState({
          ...state,
          step: 6,
        });
      }
      return true;
    } else if (state.step === 6) {
      if (action.type === "click") {
        setState({
          ...state,
          step: 7,
        });
      }
      return false;
    } else if (state.step === 7) {
      if (action.type === "click" && action?.target?.action === "buttonClick") {
        setState({
          ...state,
          step: 8,
        });
        return true;
      }
      return false;
    } else if (state.step === 8) {
      if (
        action.type === "click" &&
        action?.target?.action === "buttonClick" &&
        action?.target?.buttonAction === "accept"
      ) {
        setState({
          ...state,
          step: 9,
        });
      }
    } else if (state.step < 14) {
      if (action.type === "click") {
        setState({
          ...state,
          step: state.step + 1,
        });
      }
      return false;
    }
    return true;
  };

  return [returnStates[state.step] || {}, handleAction];
};
