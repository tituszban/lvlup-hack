import * as PIXI from "pixi.js";
import { itemTypes } from "./contants";
import Tree1 from "./assets/Tree1.png";
import Tree2 from "./assets/Tree2.png";
import Tree3 from "./assets/Tree3.png";
import Tree4 from "./assets/Tree4.png";
import Tree5 from "./assets/Tree5.png";
import Tree6 from "./assets/Tree6.png";
import Bench from "./assets/Bench.png";
import Fountain from "./assets/Fountain.png";
import Signpost from "./assets/SignPost.png";
import Crate from "./assets/Crate.png";
import Fire from "./assets/Fire.png";
import Blanket1 from "./assets/Blanket1.png";
import Blanket2 from "./assets/Blanket2.png";
import Snail from "./assets/Snail.png";
import Umbrella from "./assets/Umbrella.png";
import Clearing from "./assets/WoodedClearing.png";

import Shadow from "./assets/Shadow.png";

const defaultShadow = {
  image: Shadow,
  alpha: 0.5,
};

const items = {
  fountain: {
    image: Fountain,
    anchor: [0.5, 0.6],
    scale: 0.25,
    radius: 100,
    radiusOffset: [0, -80],
    title: "Fountain",
    type: itemTypes.static,
    hitArea: new PIXI.Polygon([
      -138, -399, 76, -396, 232, 23, 408, 73, 380, 219, -4, 274, -376, 229,
      -408, 70, -272, 27,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.5, -0.4],
      scale: 0.5,
    },
  },
  signpost: {
    image: Signpost,
    anchor: [0.5, 1],
    scale: 0.24,
    radius: 10,
    radiusOffset: [0, -20],
    title: "Signpost",
    type: itemTypes.static,
    hitArea: new PIXI.Polygon([
      35, -507, 245, -429, 146, -304, 55, -305, 43, -51, -7, -43, 4, -292, -136,
      -304, -172, -386,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.45, 1.35],
      scale: 0.12,
    },
  },
  crate: {
    image: Crate,
    anchor: [0.5, 1],
    scale: 0.2,
    radius: 20,
    radiusOffset: [0, -20],
    title: "Crate",
    type: itemTypes.static,
    hitArea: new PIXI.Polygon([
      36, -553, 336, -353, 312, -118, 10, -6, -278, -126, -320, -351, -276,
      -441,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.5, 0.95],
      scale: [0.35, 0.65],
    },
  },
  tree1: {
    image: Tree1,
    anchor: [0.5, 0.95],
    scale: 0.3,
    radius: 50,
    radiusOffset: [0, -25],
    title: "Oak tree",
    type: itemTypes.daily,
    hitArea: new PIXI.Polygon([
      -141, -900, 180, -900, 460, -530, 88, -260, 108, 5, -120, 5, -100, -300,
      -431, -440,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.5, 0.7],
      scale: 0.38,
    },
  },
  tree2: {
    image: Tree2,
    anchor: [0.5, 0.95],
    scale: 0.3,
    radius: 50,
    radiusOffset: [0, -25],
    title: "Pine tree",
    type: itemTypes.daily,
    hitArea: new PIXI.Polygon([
      65.0, -891.5, 313.0, -145.5, 71.0, -106.5, 64.0, 1.5, -96.0, 0.5, -69.0,
      -128.5, -317.0, -194.5,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.53, 0.7],
      scale: 0.25,
    },
  },
  tree3: {
    image: Tree3,
    anchor: [0.5, 0.95],
    scale: 0.3,
    radius: 50,
    radiusOffset: [0, -25],
    title: "Tall tree",
    type: itemTypes.daily,
    hitArea: new PIXI.Polygon([
      -155.5, -805.0, 135.5, -821.0, 521.5, -724.0, 135.5, -456.0, 138.5, -21.0,
      -170.5, -2.0, -71.5, -453.0, -516.5, -479.0, -388.5, -688.0,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.5, 0.7],
      scale: 0.35,
    },
  },
  tree4: {
    image: Tree4,
    anchor: [0.5, 0.95],
    scale: 0.3,
    radius: 50,
    radiusOffset: [0, -25],
    title: "Wide tree",
    type: itemTypes.daily,
    hitArea: new PIXI.Polygon([
      -376, -901, 218, -832, 606, -456, 96, -377, 162, 23, -34, 37, -18, -365,
      -600, -560,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.4, 0.3],
      scale: 0.38,
    },
  },
  tree5: {
    image: Tree5,
    anchor: [0.5, 0.95],
    scale: 0.3,
    radius: 50,
    radiusOffset: [0, -25],
    title: "Lanky tree",
    type: itemTypes.daily,
    hitArea: new PIXI.Polygon([
      -121, -1161, 81, -1193, 132, 2, -9, 6, -206, -849,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.35, 0.5],
      scale: 0.25,
    },
  },
  tree6: {
    image: Tree6,
    anchor: [0.5, 0.95],
    scale: 0.25,
    radius: 50,
    radiusOffset: [0, -25],
    title: "Sad tree",
    type: itemTypes.daily,
    hitArea: new PIXI.Polygon([
      -476, -992, 236, -1195, 706, -633, 312, -637, 412, 32, -92, 30, 154, -597, -708, -614
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.5, 0.5],
      scale: 0.65,
    },
  },
  bench: {
    image: Bench,
    anchor: [0.5, 0.4],
    scale: 0.2,
    radius: 30,
    radiusOffset: [0, 0],
    title: "Bench",
    type: itemTypes.decoration,
    hitArea: new PIXI.Polygon([
      -167.0, -248.0, 331.0, -63.0, 331.0, 319.0, 161.0, 407.0, -335.0, 122.0,
      -338.0, -34.0, -173.0, -89.0,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.4, 0],
      scale: [0.35, 0.65],
      rotation: 0.45,
    },
  },
  fire: {
    image: Fire,
    anchor: [0.5, 1],
    scale: 0.18,
    radius: 30,
    radiusOffset: [0, 0],
    title: "Fire",
    type: itemTypes.decoration,
    hitArea: new PIXI.Polygon([
      -86, -558, 12, -531, 156, -264, 152, -106, 24, -21, -134, -51, -168, -179,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.46, 1],
      scale: [0.15, 0.5],
    },
  },
  blanket1: {
    image: Blanket1,
    anchor: [0.5, 0.5],
    scale: 0.3,
    radius: 30,
    radiusOffset: [0, -80],
    title: "Blue blanket",
    type: itemTypes.decoration,
    hitArea: new PIXI.Polygon([-241, -160, 362, -124, 189, 136, -361, 120]),
    shadow: {
      ...defaultShadow,
      anchor: [1.8, 1],
      scale: [0.12, 0.3],
    },
  },
  blanket2: {
    image: Blanket2,
    anchor: [0.5, 0.5],
    scale: 0.3,
    radius: 30,
    radiusOffset: [0, -80],
    title: "Red blanket",
    type: itemTypes.decoration,
    hitArea: new PIXI.Polygon([-241, -160, 362, -124, 189, 136, -361, 120]),
    shadow: {
      ...defaultShadow,
      anchor: [1.8, 1],
      scale: [0.12, 0.3],
    },
  },
  snail: {
    image: Snail,
    anchor: [0.5, 0.5],
    scale: 0.15,
    radius: 10,
    radiusOffset: [0, -80],
    title: "Snail friend",
    type: itemTypes.decoration,
    hitArea: new PIXI.Polygon([-132, -148, 324, -128, 158, 152, -324, 122]),
    shadow: {
      ...defaultShadow,
      anchor: [0.55, -0.3],
      scale: 0.2,
    },
  },
  umbrella: {
    image: Umbrella,
    anchor: [0.5, 0.5],
    scale: 0.25,
    radius: 10,
    radiusOffset: [0, -100],
    title: "Parasol",
    type: itemTypes.decoration,
    hitArea: new PIXI.Polygon([
      -24, -242, 158, -180, 160, 242, -196, 48, -186, -130,
    ]),
    shadow: {
      ...defaultShadow,
      anchor: [0.55, -1.4],
      scale: 0.3,
    },
  },
  clearing: {
    image: Clearing,
    anchor: [0.5, 0.5],
    scale: 0.4,
    radius: 50,
    radiusOffset: [0, -80],
    title: "Clearing",
    type: itemTypes.decoration,
    hitArea: new PIXI.Polygon([20, -44, 132, 236, -362, 334]),
  },
};

export default items;
