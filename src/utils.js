const translationOffsetY = 10;
const translationScalingY = 1.8;

export const toMapFrame = ([x, y]) => {
  const _y = (y - translationOffsetY) * translationScalingY;
  return [x + _y, x - _y];
};

export const toViewFrame = ([x, y]) => {
  return [(x + y) / 2, (x - y) / 2 / translationScalingY + translationOffsetY];
};

export const clamp = (x, min, max) => {
  return Math.min(Math.max(x, min), max);
};